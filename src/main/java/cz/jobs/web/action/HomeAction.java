package cz.jobs.web.action;

import cz.jobs.model.DBModel;
import net.sourceforge.stripes.action.*;

/**
 * Titulni strana
 * Created by pivko on 30.10.2014.
 */
@UrlBinding("/home.action")
public class HomeAction extends BaseAction {

    @DefaultHandler
    public Resolution show() {
        return jspResolution("home.jsp");
    }
}
