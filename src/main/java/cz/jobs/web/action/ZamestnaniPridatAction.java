package cz.jobs.web.action;

import cz.jobs.model.DBModel;
import cz.jobs.model.NabidkaModel;
import cz.jobs.model.UchazecModel;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;

/**
 * Created by pivko on 30.10.2014.
 */
public class ZamestnaniPridatAction extends BaseAction {
    @ValidateNestedProperties({
            @Validate(field="nazev", required=true)
    })
    private NabidkaModel record;


    public NabidkaModel getRecord() { return record; }
    public void setRecord(NabidkaModel r) { this.record = r; }


    @DefaultHandler
    @DontValidate
    public Resolution show() {
        return jspResolution("zamestnani-pridat.jsp");
    }

    public Resolution save(){
        // zapis do databaze
        DBModel.addRecordNabidky(record);

        return jspResolution("zamestnani-pridat.jsp");
    }
}