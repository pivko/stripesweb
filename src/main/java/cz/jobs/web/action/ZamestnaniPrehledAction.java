package cz.jobs.web.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;

/**
 * Created by pivko on 30.10.2014.
 */
public class ZamestnaniPrehledAction extends BaseAction {
    @DefaultHandler
    public Resolution show() {
        return jspResolution("zamestnani-prehled.jsp");
    }
}
