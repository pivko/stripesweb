<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<stripes:layout-render name="/WEB-INF/jsp/layouts/default.jsp" pageTitle="Prehled nabidek">
    <stripes:layout-component name="contents">
        <h1>uchazeci prehled</h1>
        <jsp:useBean id="record" class="cz.jobs.model.DBModelMapper" />
        <ul>
            <c:forEach items="${record.recordsNabidka}" var="rec" >
                <li>
                    <stripes:link beanclass="cz.jobs.web.action.ShowDetailNabidkaAction" event="show">
                        <stripes:param name="recordId" value="${rec.id}"/>
                        ${rec.nazev}
                    </stripes:link>
                </li>
            </c:forEach>
        </ul>
    </stripes:layout-component>
</stripes:layout-render>


