package cz.jobs.web.action;

import cz.jobs.model.DBModel;
import cz.jobs.model.UchazecModel;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;

import javax.mail.Session;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

/**
 * Created by pivko on 30.10.2014.
 */
public class UchazeciPridatAction extends BaseAction {
    @ValidateNestedProperties({
            @Validate(field="jmeno", required=true)
    })
    private UchazecModel record;


    public UchazecModel getRecord() { return record; }
    public void setRecord(UchazecModel r) { this.record = r; }


    @DefaultHandler
    @DontValidate
    public Resolution show() {
        return jspResolution("uchazeci-pridat.jsp");
    }

    public Resolution save(){
        // zapis do databaze
        DBModel.addRecordUchazeci(record);

        return jspResolution("uchazeci-pridat.jsp");
    }
}