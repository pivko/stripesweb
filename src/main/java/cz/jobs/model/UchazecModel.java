package cz.jobs.model;

/**
 * Created by pivko on 2.11.2014.
 */
public class UchazecModel {
    private Integer id;
    private String jmeno;
    private Integer vek;

    public String getJmeno()            {        return jmeno;    }
    public void setJmeno(String jmeno)  {        this.jmeno = jmeno;    }

    public Integer getId()         {        return id;    }
    public void setId(Integer id)  {        this.id = id;    }

    public Integer getVek()         {        return vek;    }
    public void setVek(Integer vek)  {        this.vek = vek;    }


}
