<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<stripes:layout-render name="/WEB-INF/jsp/layouts/default.jsp" pageTitle="Pridat uchazece">
    <stripes:layout-component name="contents">

<h1>uchazeci pridat</h1>

    <stripes:form beanclass="cz.jobs.web.action.UchazeciPridatAction" focus="">
        <stripes:errors/>
            <table>
                <tr>
                    <td>jmeno a prijmeni</td>
                    <td><stripes:text name="record.jmeno"/></td>
                </tr>
                <tr>
                    <td>vek</td>
                    <td><stripes:text name="record.vek"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <stripes:submit name="save" value="Pridat"/>
                    </td>
                </tr>
            <td>${actionBean.record.jmeno}</td>
        </table>
    </stripes:form>

</stripes:layout-component>
</stripes:layout-render>

