package cz.jobs.web.action;

import cz.jobs.model.DBModel;
import cz.jobs.model.UchazecModel;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;

/**
 * Created by pivko on 2.11.2014.
 */
public class ShowDetailUchazecAction extends BaseAction {
    private UchazecModel record;
    private Integer recordId;

    public UchazecModel getRecord() { return record; }
    public void setRecord(UchazecModel r) { this.record = r; }

    public Integer getRecordId() { return recordId; }
    public void setRecordId(Integer r) { this.recordId = r; }

    @DefaultHandler
    public Resolution show() {
        record = DBModel.getUchazecModelFromId(recordId);
        return jspResolution("uchazec-detail.jsp");
    }
}
