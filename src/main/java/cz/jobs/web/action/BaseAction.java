package cz.jobs.web.action;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 * Created by pivko on 30.10.2014.
 */
public class BaseAction implements ActionBean {

    public static final String JSP_DIR = "/WEB-INF/jsp/";

    private ActionBeanContext context;

    public ActionBeanContext getContext() { return context; }
    public void setContext(ActionBeanContext context) { this.context = context; }

    protected Resolution jspResolution(String pageName){
        return new ForwardResolution(JSP_DIR+pageName);
    }
}
