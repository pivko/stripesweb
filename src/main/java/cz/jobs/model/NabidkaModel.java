package cz.jobs.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
/**
 * Created by pivko on 3.11.2014.
 * jedeme pres objectify
 */
@Entity
public class NabidkaModel {
    @Id private Long id;
    private String nazev;

    public String getNazev(){
        return nazev;
    }
    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }
}
