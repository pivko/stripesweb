<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld" %>
<stripes:layout-render name="/WEB-INF/jsp/layouts/default.jsp" pageTitle="Home">
    <stripes:layout-component name="contents">
        <h3>Vyhrazeno právníkům i studentům</h3>
        <ul>
            <li>uplatnění v průběhu studia</li>
            <li>uplatnění po ukončení studia</li>
            <li>pracovní místa</li>
            <li>bezplatné výpomoce nebo praxe</li>
            <li>stáže a další aktivity</li>
        </ul>
        <p>
            V tomto zkušebním provozu nabízíme speciální nabídku a poptávku zaměstnání zaměřenou na právnickou obec studentskou, čerstvě absolventskou, i koneckonců uplatnění pro právníky s praxí.
        </p>
        <p>
            Je tedy určena pro právníky i studenty práv obecně, přičemž každý si může určit charakter nabízené či poptávané činnosti. Zvláště doporučujeme studentům, kteří hledají možnost placené či neplacené praxe v průběhu studia jako praxe odborné, tak absolventům poptávajícím trvalý pracovní poměr.
        </p>
    </stripes:layout-component>
</stripes:layout-render>