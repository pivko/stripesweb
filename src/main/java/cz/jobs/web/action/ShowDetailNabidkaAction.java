package cz.jobs.web.action;

import cz.jobs.model.DBModel;
import cz.jobs.model.NabidkaModel;
import cz.jobs.model.UchazecModel;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;

/**
 * Created by pivko on 3.11.2014.
 */
public class ShowDetailNabidkaAction extends BaseAction {
    private NabidkaModel record;
    private Integer recordId;

    public NabidkaModel getRecord() { return record; }
    public void setRecord(NabidkaModel r) { this.record = r; }

    public Integer getRecordId() { return recordId; }
    public void setRecordId(Integer r) { this.recordId = r; }

    @DefaultHandler
    public Resolution show() {
        record = DBModel.getNabidkaModelFromId(recordId);

        return jspResolution("nabidka-detail.jsp");
    }
}
