<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld" %>
<stripes:layout-render name="/WEB-INF/jsp/layouts/default.jsp" pageTitle="Detail uchazece">
    <stripes:layout-component name="contents">
        <h1>Detail uchazece: ${actionBean.record.jmeno}, DB ID ${actionBean.record.id} </h1>
    </stripes:layout-component>
</stripes:layout-render>

