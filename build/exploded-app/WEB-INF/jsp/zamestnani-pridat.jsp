<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<stripes:layout-render name="/WEB-INF/jsp/layouts/default.jsp" pageTitle="Pridat zamestnance">
    <stripes:layout-component name="contents">

<h1>Zamestani pridat</h1>

<stripes:form beanclass="cz.jobs.web.action.ZamestnaniPridatAction" focus="">
    <stripes:errors/>
    <table>
        <tr>
            <td>nazev</td>
            <td><stripes:text name="record.nazev"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <stripes:submit name="save" value="Pridat"/>
            </td>
        </tr>
        <td>${actionBean.record.nazev}</td>
    </table>
</stripes:form>
</stripes:layout-component>
</stripes:layout-render>