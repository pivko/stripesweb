package cz.jobs.model;

import com.googlecode.objectify.ObjectifyService;

import static com.googlecode.objectify.ObjectifyService.ofy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Simuluje databazi pres Session.
 * Created by pivko on 2.11.2014.
 */
public class DBModel {
    static{
        ObjectifyService.register(NabidkaModel.class);
    }
    private static List<UchazecModel> recordsUchazeci=new ArrayList<UchazecModel>();


    public static List<UchazecModel> getRecordsUchazeci(){
        return recordsUchazeci;
    }
    public static int getUchazecNextId(){
        return recordsUchazeci.size()+1;
    }
    public static void addRecordUchazeci(UchazecModel rec){
        rec.setId(getUchazecNextId());
        recordsUchazeci.add(rec);
    }
    public static UchazecModel getUchazecModelFromId(int id){
        for (int i = 0; i < recordsUchazeci.size(); i++) {
            UchazecModel r = recordsUchazeci.get(i);
            if(r.getId() == id){
                return r;
            }
        }
        return null;
    }


    /* TATO CAST RESENA OBJECTIFY */
    public static List<NabidkaModel> getRecordsNabidky(){
        return ofy().load().type(NabidkaModel.class).list();
    }
    public static Long getNabidkaNextId(){
        return new Long(getRecordsNabidky().size()+1 );
    }
    public static void addRecordNabidky(NabidkaModel rec){
        rec.setId(getNabidkaNextId());
        ofy().save().entity(rec).now();
    }
    public static NabidkaModel getNabidkaModelFromId(int id){
        return ofy().load().type(NabidkaModel.class).id(id).now();
    }


}
