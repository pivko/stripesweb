package cz.jobs.web.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;

/**
 * Created by pivko on 30.10.2014.
 */
public class UchazeciPrehledAction extends BaseAction {

    @DefaultHandler
    public Resolution show() {
        return jspResolution("uchazeci-prehled.jsp");
    }
}