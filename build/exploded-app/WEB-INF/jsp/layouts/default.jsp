<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes-dynattr.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<stripes:layout-definition>

    <%@
        page isELIgnored="false" contentType="text/html; charset=UTF-8" language="java" %><%@
        taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %><%@
        taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>${pageTitle}</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/default.css" />
    <stripes:layout-component name="html_head" />
</head>

<body>
    <stripes:layout-component name="header">
        <c:import url="/WEB-INF/jsp/layouts/header.jsp" />
    </stripes:layout-component>
    <stripes:layout-component name="mainmenu">
        <c:import url="/WEB-INF/jsp/layouts/mainmenu.jsp" />
    </stripes:layout-component>

    <div class="pageContent">
        <stripes:layout-component name="contents" />
    </div>

    <stripes:layout-component name="footer" >
        <c:import url="/WEB-INF/jsp/layouts/footer.jsp" />
    </stripes:layout-component>
</body>
</html>
</stripes:layout-definition>