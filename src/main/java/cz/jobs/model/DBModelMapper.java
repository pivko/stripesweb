package cz.jobs.model;

import java.util.ArrayList;
import java.util.List;

/** Zastupna trida pro DBModel, protoze staticke promenne nemuzeme mapovat ve Stripes na formulare
 * Created by pivko on 2.11.2014.
 */
public class DBModelMapper {
    private List<UchazecModel> recordsUchazeci=new ArrayList<UchazecModel>();
    private List<NabidkaModel> recordsNabidka=new ArrayList<NabidkaModel>();

    public List<UchazecModel> getRecordsUchazeci(){ return recordsUchazeci;}
    public void setRecordsUchazeci(List<UchazecModel> a){ this.recordsUchazeci = a;}

    public List<NabidkaModel> getRecordsNabidka(){ return recordsNabidka;}
    public void setRecordsNabidka(List<NabidkaModel> a){ this.recordsNabidka = a;}

    public DBModelMapper(){
        recordsUchazeci = DBModel.getRecordsUchazeci();
        recordsNabidka  = DBModel.getRecordsNabidky();
    }
}
